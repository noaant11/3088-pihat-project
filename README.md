# 3088 PiHat Project

Irrigation PiHat project description

#PiHat Description

The irrigation PiHAT can be used for the irrigation of small gardens/vegetable patches via soil moisture level sensing and water dispensing.

The moisture sensors embedded in the soil read analogue voltage and provide this signal to the PiHAT via input pins. Watering levels are controlled by the user via a PCB mounted potentiometer. 12VDC valves (in other configurations this could be another type of device) are connected to the PiHAT for water (or fertilizer) distribution. The higher voltage power supply provides power to the PiHAT separately from the main Pi board as the valves draw far more current and voltage than the Pi can supply from its GPIO header. Each moisture sensor will have an LED level indicator (Red, Orange, Green) to show soil moisture levels, where Red indicates dry soil and all three LEDs indicate suitably dampened soil

#Bill of Materials
x2 Valves
Analogue moisture sensor
Switching power supply circuitry
x3 LEDs, R,O,G
Float Switch
Humidity sensor
Comparators
